//
//  TrackTableViewCell.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/4/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import UIKit
import SDWebImage

class TrackTableViewCell: UITableViewCell {

    var track:Track?
    
    var indexPath: IndexPath? {
        return (superview as? UITableView)?.indexPath(for: self)
    }
    
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var camelotLabel: UILabel!
    
    public static func createTrackTableViewCell(tableview:UITableView, reuseIdentifier:String, indexPath:IndexPath, track:Track) -> TrackTableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! TrackTableViewCell
        cell.track = track
        cell.setUIElements()
        return cell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension TrackTableViewCell {
    fileprivate func setUIElements() {
        let url = URL(string: (track?.PicUrlM)!)
        albumImageView.backgroundColor = UIColor.darkGray
        albumImageView.sd_setShowActivityIndicatorView(true)
        albumImageView.sd_setIndicatorStyle(.whiteLarge)
        albumImageView.sd_setImage(with: url)
        songNameLabel.text = track?.Name
        artistNameLabel.text = track?.Artist
        bpmLabel.text = String(describing: (track?.BPM) ?? 0)
        if let label = keyLabel {
            label.text = track?.Key
        }
        if let label = camelotLabel {
            label.text = track?.Camelot
        }
    }
}
