//
//  Track.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/3/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import UIKit

class Track {
    var Acousticness:Int?
    var Artist:String?
    var BPM:Int?
    var Camelot:String?
    var Danceability:Int?
    var Duration:String?
    var Energy:Int?
    var Happiness:Int?
    var Id:String?
    var Instumentalness:Int?
    var Key:String?
    var Liveness:Int?
    var Name:String?
    var PicUrlL:String?
    var PicUrlM:String?
    var PicUrlS:String?
    var Popularity:Int?
    var PreviewUrl:String?
    var Recommendations:[Track]?
    var SkbLink:String?
    var Speechiness:Int?
    var Url:String?
    
    init(object:[String:Any]) {
        Acousticness = Int((object["Acousticness"] as? String) ?? "0")
        Artist = object["Artist"] as? String
        BPM = Int((object["BPM"] as? String) ?? "0")
        Camelot = object["Camelot"] as? String
        Danceability = Int((object["Danceability"] as? String) ?? "0")
        Duration = object["Duration"] as? String
        Energy = Int((object["Energy"] as? String) ?? "0")
        Happiness = Int((object["Happiness"] as? String) ?? "0")
        Id = object["Id"] as? String
        Instumentalness =  Int((object["Instumentalness"] as? String) ?? "0")
        Key =  object["Key"] as? String
        Liveness =  Int((object["Liveness"] as? String) ?? "0")
        Name =  object["Name"] as? String
        PicUrlL =  object["PicUrlL"] as? String
        PicUrlM = object["PicUrlM"] as? String
        PicUrlS = object["PicUrlS"] as? String
        Popularity = Int((object["Popularity"] as? String) ?? "0")
        PreviewUrl = object["PreviewUrl"] as? String
        SkbLink = object["SkbLink"] as? String
        Speechiness = Int((object["Speechiness"] as? String) ?? "0")
        Url = object["Url"] as? String
    }
}
