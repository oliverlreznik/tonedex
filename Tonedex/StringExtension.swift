//
//  StringExtension.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/3/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import Foundation

extension String {
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
