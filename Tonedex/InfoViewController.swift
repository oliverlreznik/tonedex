//
//  InfoViewController.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/4/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import UIKit
import SDWebImage

class InfoViewController: UIViewController {
    
    public var track:Track?
    public var recommendations:[Track]?

    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var camelotLabel: UILabel!
    
    @IBOutlet weak var attributesButton: UIButton!
    @IBAction func attributesTap(_ sender: Any) {
        switchTab(button: attributesButton, tab: view)
    }
    @IBOutlet weak var mixingButton: UIButton!
    @IBAction func mixingTap(_ sender: Any) {
        switchTab(button: mixingButton, tab: view)
    }
    @IBOutlet weak var listenButton: UIButton!
    @IBAction func listenTap(_ sender: Any) {
        switchTab(button: listenButton, tab: view)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var intermediateView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        if let track = track {
            songNameLabel.text = track.Name
            artistNameLabel.text = track.Artist
            albumImageView.sd_setImage(with: URL(string: track.PicUrlM!))
            bpmLabel.text = String(describing: (track.BPM)!)
            durationLabel.text = track.Duration
            keyLabel.text = track.Key
            camelotLabel.text = track.Camelot
            
            SKBRequester.GetRecommendations(id: (track.Id)!) { (data, error) in
                if error {
                    print(error)
                } else {
                    self.recommendations = []
                    for track in data {
                        self.recommendations?.append(Track(object: track as! [String : Any]))
                    }
                    DispatchQueue.main.async{
                        self.tableView.reloadData()
                        self.intermediateView.isHidden = true
                    }
                }
            }
        }
        
        switchTab(button: attributesButton, tab: view)
        activityIndicator.startAnimating()
        
        tableView.delegate = self
        tableView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension InfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recommendations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return TrackTableViewCell.createTrackTableViewCell(tableview: tableView, reuseIdentifier: "TrackRID", indexPath: indexPath, track: recommendations![indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MStoI", sender: tableView.cellForRow(at: indexPath))
    }

    
}

extension InfoViewController {
    fileprivate func switchTab(button:UIButton, tab:UIView) {
        let buttons = [attributesButton, mixingButton, listenButton]
        
        let width:CGFloat = 1.5
        let unselectedButtonColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
        
        for item in buttons {
            item?.layer.borderWidth = 0.0
            if button != item {
                item?.layer.addBorder(edge: UIRectEdge.top, color: unselectedButtonColor, thickness: width)
                item?.layer.addBorder(edge: UIRectEdge.right, color: unselectedButtonColor, thickness: width)
                item?.layer.addBorder(edge: UIRectEdge.left, color: unselectedButtonColor, thickness: width)
                item?.layer.addBorder(edge: UIRectEdge.bottom, color: .black, thickness: width)
                item?.backgroundColor = unselectedButtonColor
            }
        }
        
        button.layer.addBorder(edge: UIRectEdge.bottom, color: .white, thickness: width)
        button.layer.addBorder(edge: UIRectEdge.top, color: .black, thickness: width)
        button.layer.addBorder(edge: UIRectEdge.right, color: .black, thickness: width)
        button.layer.addBorder(edge: UIRectEdge.left, color: .black, thickness: width)
        
        button.backgroundColor = .white
    }
}
