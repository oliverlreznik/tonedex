//
//  SKBRequester.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/2/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import Foundation

class SKBRequester {
    
    public static func BasicSearch(q:String, completion: @escaping ([Any], Bool) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let address = "http://songkeybpm.com/ISearch?q=\(q.trim())"
                let escapedAddress = address.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
                let result = try Data(contentsOf: URL(string: escapedAddress)!)
                let json = try? JSONSerialization.jsonObject(with: result, options: [])
                if let array = json as? [Any] {
                    completion(array, false)
                } else {
                    completion([], true)
                }
            } catch {
                completion([], true)
            }
        }
    }
    
    public static func GetRecommendations(id:String, completion: @escaping ([Any], Bool) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let address = "http://songkeybpm.com/IRec?id=\(id.trim())"
                let escapedAddress = address.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
                let result = try Data(contentsOf: URL(string: escapedAddress)!)
                let json = try? JSONSerialization.jsonObject(with: result, options: [])
                if let array = json as? [Any] {
                    completion(array, false)
                } else {
                    completion([], true)
                }
            } catch {
                completion([], true)
            }
        }
    }
    
}
