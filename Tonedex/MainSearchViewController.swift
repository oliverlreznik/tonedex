//
//  FirstViewController.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/2/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class MainSearchViewController: UIViewController {
    
    var tracks:[Track] = []

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var intermediateView: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        //self.hidesBottomBarWhenPushed = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(tableView, delay: 0, scrollSpeedFactor: 2)
        }
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.showNavbar(animated: true)
        }
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.showNavbar(animated: true)
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let infoViewController = segue.destination as! InfoViewController
        let cell = sender as! TrackTableViewCell
        infoViewController.track = cell.track
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension MainSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return TrackTableViewCell.createTrackTableViewCell(tableview: tableView, reuseIdentifier: "TrackRID", indexPath: indexPath, track: tracks[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MStoI", sender: tableView.cellForRow(at: indexPath))
    }

}

extension MainSearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        intermediateView.isHidden = false
        intermediateView.backgroundColor = UIColor.black
        intermediateView.alpha = 0.7
        indicatorView.isHidden = false
        indicatorView.startAnimating()
        instructionsLabel.isHidden = true
        
        
        SKBRequester.BasicSearch(q: searchField.text!) { (data, error) in
            if error {
                print(error)
            } else {
                self.tracks = []
                for track in data {
                    self.tracks.append(Track(object: track as! [String : Any]))
                }
                DispatchQueue.main.async{
                    self.intermediateView.isHidden = true
                    self.intermediateView.backgroundColor = UIColor.darkGray
                    self.indicatorView.isHidden = true
                    self.tableView.reloadData()
                }
            }
        }
        
        return true
    }
}
