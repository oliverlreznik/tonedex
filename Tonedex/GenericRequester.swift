//
//  GenericRequester.swift
//  Tonedex
//
//  Created by Oliver Reznik on 8/4/17.
//  Copyright © 2017 Oliver. All rights reserved.
//

import Foundation

class GenericRequester {
    
    public static func GetImage(urlString:String, completion: @escaping (UIImage, Bool) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let result = try Data(contentsOf: URL(string:urlString)!)
                let image = UIImage(data: result)
                if let image = image {
                    completion(image, false)
                } else {
                    completion(#imageLiteral(resourceName: "Placeholder Album"), true)
                }
            } catch {
                completion(#imageLiteral(resourceName: "Placeholder Album"), true)
            }
        }
    }
    
}
